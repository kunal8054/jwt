const jwt = require('jsonwebtoken')
const User = require('../models/user_jwt')
const common = require('../utility/common')

const auth = async (req, res, next) => {

    try {

        const token = req.header('Authorization').replace('Bearer ', '')
        const decodedToken = await jwt.verify(token, process.env.JWT_SECRET)

        const user = await User.findOne({ _id: decodedToken._id, 'tokens.token': token })
        if (!user) {
            throw new Error()
        }
        req.token = token
        req.user = user

        next()

    } catch (error) {
        common.sendError(res, 'Please Authenticate!')
    }

}

module.exports = auth