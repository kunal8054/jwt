const mongoose = require('mongoose')

const testSchema = new mongoose.Schema({
    name: {
        first: string,
        last: string
    }
})

const Test = mongoose.model('test', testSchema)

module.exports = Test