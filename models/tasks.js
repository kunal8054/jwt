const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({

    description: {
        type: 'string',
        required: true
    },
    completed: {
        type: 'boolean',
        default: false
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        // required: true,
        ref: 'User'
    }

}, {
    timestamps: true
})


const Task = mongoose.model('task', userSchema)

module.exports = Task