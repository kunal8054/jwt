const mongoose = require('mongoose')
const validator = require('validator')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')


const userJWTSchema = new mongoose.Schema({

    name: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true,
        lowerCase: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error("Invalid email")
            }
        }
    },
    age: {
        type: Number,
        required: true,
        default: 0,
        validate(value) {
            if (value < 0) {
                throw new Error("Invalid Age")
            }
        }
    },
    password: {
        type: String,
        required: true,
        trim: true
    },
    address: {
        type: String,
        required: true,
        trim: true
    },
    tokens: [{
        token: {
            type: String,
            required: true,
        }
    }]

}, {
    timestamps: true,
})


// statics are method defined on models
// methods are defined on documents(instance) 


userJWTSchema.methods.generateAuthToken = async function () {
    const user = this
    // const token = jwt.sign({ _id: user._id.toString() }, 'hello', { expiresIn: '1200000s' })
    const token = jwt.sign({ _id: user._id.toString() }, process.env.JWT_SECRET)

    user.tokens = user.tokens.concat({ token })
    await user.save()
    return token
}


userJWTSchema.statics.findByCredentials = async (email, password) => {

    const user = await UserJwt.findOne({ email: email })
    if (!user) {
        throw new Error("Unable to logIn")
    }

    const isMatch = await bcrypt.compare(password, user.password)
    if (!isMatch) {
        throw new Error("Unable to logIn")
    }
    return user
}



// hashing password before saving
userJWTSchema.pre('save', async function (next) {

    const user = this
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 10)
    }
    next()
})





const UserJwt = mongoose.model('userjwt', userJWTSchema)

module.exports = UserJwt