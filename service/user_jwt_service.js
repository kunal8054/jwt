const User = require('../models/user_jwt')


// Sign Up
module.exports.signUp = async (body) => {

    const user = await new User(body)
    await user.save()

    // It internally calls save() function
    // const user = await User.create(body)

    // const token = await user.generateAuthToken()
    // return { user, token }
    return user
}


//Sign In
module.exports.signIn = async (email, password) => {

    const user = await User.findByCredentials(email, password)
    const token = await user.generateAuthToken()

    return { user, token }
}


// Profile 
module.exports.profile = async (user) => {

    return user
}




// Logout
module.exports.logOut = async (req) => {

    req.user.tokens = req.user.tokens.filter((token) => {
        return token.token !== req.token
    })
    await req.user.save()
    return req.user
}


// Logout all
module.exports.logOutAll = async (req) => {

    req.user.tokens = []
    await req.user.save()

    return req.user
}



// Update user
module.exports.updateJwtUser = async (user_id, body) => {

    const updates = Object.keys(body)
    const allowedUpdates = ['name', 'age', 'email']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

    if (!isValidOperation) {
        throw new Error('Invalid update operation')
    }

    const user = await User.findById(user_id)

    if (!user) {
        throw new Error('User not found')
    }
    updates.forEach((update) => user[update] = body[update])

    await user.save()
    return user
}
