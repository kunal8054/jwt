
const Task = require('../models/tasks')


module.exports.addTask = async (body) => {

    const task = new Task({
        ...body,
        // owner: req.user
    })
    return await task.save()

}



module.exports.getAllTasks = async () => {

    const tasks = await Task.find({})
    if (tasks.length <= 0) {
        throw new Error('No tasks found')
    }
    return tasks

}



module.exports.deleteTask = async (id) => {

    const task = await Task.findById(id)

    if (!task) {
        throw new Error('Invalid delete request')
    }

    await task.remove()
    return task

}



module.exports.updateTask = async (id, body) => {

    const updates = Object.keys(body)
    const allowedUpdates = ['description', 'completed']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

    if (!isValidOperation) {
        throw new Error('Invalid update operation')
    }

    const task = await Task.findById(id)

    if (!task) {
        throw new Error('Task not found')
    }

    updates.forEach((update) => task[update] = body[update])

    await task.save()
    return task

}



module.exports.upsertService = async (id, body) => {

    await Task.findOneAndUpdate({
        _id: id
    }, {
        $set: body
    }
    )
}


module.exports.leanService = async (id) => {

    const task = await Task.findOne({ _id: id })
    task.completed = "first"
    console.log(task)

    const task2 = await Task.findOne({ _id: id }).lean()
    // console.log(task2)
    task2.completed = "testing"
    console.log(task2)

}