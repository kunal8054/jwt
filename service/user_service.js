const User = require('../models/users')


// Save User
module.exports.saveUser = async (name, age, email) => {

    const user = await new User({
        name,
        age,
        email
    })

    return await user.save()

}


// Get all users 
module.exports.getAllUsers = async () => {

    const users = await User.find({})

    if (!users) {
        throw new Error('Error getting users')
    }
    return users
}




// Delete user 
module.exports.deleteUser = async (user_id) => {

    const user = await User.findById(user_id)

    if (!user) {
        throw new Error('Error Deleting users')
    }
    await user.remove()
    return user
}



// Update user
module.exports.updateUser = async (user_id, body) => {

    const updates = Object.keys(body)
    const allowedUpdates = ['name', 'age', 'email']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

    if (!isValidOperation) {
        throw new Error('Invalid update operation')
    }

    const user = await User.findById(user_id)

    if (!user) {
        throw new Error('User not found')
    }
    updates.forEach((update) => user[update] = body[update])

    await user.save()
    return user
}


