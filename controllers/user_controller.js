const userService = require('../service/user_service')
const userServiceJWT = require('../service/user_jwt_service')

const common = require('../utility/common')





//get all users
module.exports.getAllUsers = async (req, res) => {

    try {
        const users = await userService.getAllUsers()
        common.sendSuccess(res, users)

    } catch (error) {
        common.sendError(res, error.message)
    }
}


// Create user
module.exports.createUser = async (req, res) => {

    const name = req.body.name
    const age = req.body.age
    const email = req.body.email

    if (!name || !age || !email) {
        common.sendError(req, 'All fields are required')
    }

    try {
        const user = await userService.saveUser(name, age, email)
        common.sendSuccess(req, user)
    } catch (error) {
        return common.sendError(res, error.message)
    }
}


// Delete User
module.exports.deleteUser = async (req, res) => {

    try {
        const user = await userService.deleteUser(req.params.id)
        common.sendSuccess(req, user)

    } catch (error) {
        common.sendError(req, error.message)
    }
}


// Update User
module.exports.updateUser = async (req, res) => {

    try {
        const user = await userService.updateUser(req.params.id, req.body)
        common.sendSuccess(res, user)

    } catch (error) {
        common.sendError(res, error.message)
    }
}



// JWT Controllers


// Sign Up
module.exports.signUp = async (req, res) => {

    try {
        const user = await userServiceJWT.signUp(req.body)
        common.sendSuccess(res, user)
    } catch (err) {
        common.sendError(res, err.message)
    }
}


// Sign In
module.exports.signIn = async (req, res) => {

    try {
        const user = await userServiceJWT.signIn(req.body.email, req.body.password)
        common.sendSuccess(res, user)
    } catch (err) {
        common.sendError(res, err.message)
    }

}


// Profile
module.exports.profile = async (req, res) => {

    try {
        const user = await userServiceJWT.profile(req.user)
        common.sendSuccess(res, user)
    } catch (err) {
        common.sendError(res, err.message)
    }

}



// Logout
module.exports.logOut = async (req, res) => {

    try {
        const user = await userServiceJWT.logOut(req)
        common.sendSuccess(res, user)
    } catch (err) {
        common.sendError(res, err.message)
    }

}


// Logout all
module.exports.logOutAll = async (req, res) => {

    try {
        const user = await userServiceJWT.logOutAll(req)
        common.sendSuccess(res, user)
    } catch (err) {
        common.sendError(res, err.message)
    }

}


// Update User
module.exports.updateJwtUser = async (req, res) => {

    try {

        if (req.user.id !== req.params.id) {
            throw new Error('Invalid request')
        }
        const user = await userServiceJWT.updateJwtUser(req.params.id, req.body)
        common.sendSuccess(res, user)

    } catch (error) {
        common.sendError(res, error.message)
    }
}
