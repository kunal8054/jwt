const taskService = require('../service/task_service')
const common = require('../utility/common')



module.exports.getAllTask = async (req, res) => {

    try {
        const tasks = await taskService.getAllTasks()
        common.sendSuccess(res, tasks)

    } catch (error) {
        common.sendError(res, error.message)
    }

}


module.exports.createTask = async (req, res) => {

    try {

        // console.log(req.body)
        // console.log(req.user)
        const tasks = await taskService.addTask(req.body)
        common.sendSuccess(res, tasks)

    } catch (error) {
        common.sendError(res, error.message)
    }
}


module.exports.updateTask = async (req, res) => {

    try {

        const task = await taskService.updateTask(req.params.id, req.body)
        common.sendSuccess(res, task)

    } catch (error) {
        common.sendError(res, error.message)
    }

}


module.exports.deleteTask = async (req, res) => {

    try {

        const task = await taskService.deleteTask(req.params.id)
        common.sendSuccess(res, task)

    } catch (error) {
        common.sendError(res, error.message)
    }

}




module.exports.upsertFunc = async (req, res) => {

    try {
        const task = await taskService.upsertService(req.params.id, req.body)
        common.sendSuccess(res, 'Updated Successfully')

    } catch (error) {
        common.sendError(res, error.message)
    }

}



module.exports.testLeanFunc = async (req, res)=>{

    try{

        const task = await taskService.leanService(req.params.id)
        common.sendSuccess(res, task)

    }catch(error){
        common.sendError(req, error.message)
    }

}