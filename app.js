require("dotenv").config()
const express = require('express')
require('./connection/db')


const app = express()
const port = process.env.PORT

app.use(express.json())


app.use('/',require('./routes/index'))








app.listen(port, (error) => {
    if (error) {
        return console.log('0Error starting server \n' + error)
    }
    console.log('Server is up and running on port: ' + port)
})
