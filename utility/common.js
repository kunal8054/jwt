
function sendSuccess(res, data) {
    res.status(200).send(data);
}


function sendError(res, error) {
    res.status(400).send({
        error: error
    })
}

module.exports = {
    sendSuccess: sendSuccess,
    sendError: sendError
}