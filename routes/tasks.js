const express = require('express')

const router = new express.Router()

const taskController = require('../controllers/task_controller')

router.get('/all', taskController.getAllTask)

router.post('/create', taskController.createTask)

router.put('/update/:id', taskController.updateTask)

router.delete('/delete/:id', taskController.deleteTask)

router.put('/testUpsert/:id',taskController.upsertFunc)

router.get('/testLean/:id',taskController.testLeanFunc)







module.exports = router