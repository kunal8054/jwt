const express = require('express')
const router = new express.Router()

const auth = require('../middleware/auth')

const userController = require('../controllers/user_controller')

//Get all users
router.get('/users', userController.getAllUsers)

// Create new user
router.post('/users', userController.createUser)

// Delete user
router.delete('/users/:id', userController.deleteUser)

// Update user
router.put('/users/:id', userController.updateUser)





// Jwt Routes

//Sign Up
router.post('/users/signUp', userController.signUp)

//Sign In
router.post('/users/signIn', userController.signIn)

// profile
router.get('/users/profile', auth, userController.profile)

//Logout
router.post('/users/logout', auth, userController.logOut)

//Logout all
router.post('/users/logoutAll', auth, userController.logOutAll)

//Update user
router.put('/users/jwt/:id', auth, userController.updateJwtUser)




module.exports = router